# Hovercrowd on Drupal

- Using [Phase2 Grunt Drupal Tasks](https://github.com/phase2/grunt-drupal-tasks) as starter

## Required
- [NodeJS](http://nodejs.org/download/)
- [npm](https://www.npmjs.org/)
- [Grunt](http://gruntjs.com/getting-started)
- [Composer](https://getcomposer.org)

## File Structure

`src`

- `hovercrowd.make`: Add contributed modules here.
- `modules`: Add custom modules here.
- `profiles`: Add custom profiles here. `profiles/hovercrowd` is used.
- `sites`: Drupal's sites directory. Maps to `sites` in Drupal. Note that `sites/all` is not contained here; this is normal.
    - `sites/default`: Contains `settings.php`.
- `themes`: Add custom theme here. Maps to `sites/all/themes/custom`.

`features`

- Behat tests go here.