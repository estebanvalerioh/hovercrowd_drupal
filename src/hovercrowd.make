core = 7.x
api = 2

; Drupal Core
projects[drops-7][type] = core
projects[drops-7][download][type] = git
projects[drops-7][download][branch] = master
projects[drops-7][download][url] = https://github.com/pantheon-systems/drops-7.git

; Patch to fix AJAX auto-complete alert error on early submit.
projects[drops-7][patch][254477] = http://www.drupal.org/files/autocomplete-cancel-254477-8.patch

; =====================================
; Contrib Modules
; =====================================

projects[ctools][type] = module
projects[ctools][version] = 1.4
projects[ctools][subdir] = contrib

projects[entity][type] = module
projects[entity][version] = 1.5
projects[entity][subdir] = contrib

projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = 2.x-dev
projects[jquery_update][download][type] = git
projects[jquery_update][download][branch] = 7.x-2.x
projects[jquery_update][download][url] = http://git.drupal.org/project/jquery_update.git
; jQuery Update overdue for release.
projects[jquery_update][download][revision] = 65eecb0f1fc69cf6831a66440f72e33a1effb1f3
; jQuery effects code is broken.
projects[jquery_update][patch][2123973] = http://www.drupal.org/files/issues/jquery_effects_missing-2123973-3_0.patch

projects[references_dialog][type] = module
projects[references_dialog][subdir] = contrib
projects[references_dialog][version] = 1.0-beta1

projects[entityreference][type] = module
projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.1

projects[link][type] = module
projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[date][type] = module
projects[date][subdir] = contrib
projects[date][version] = 2.8

projects[reference_option_limit][version] = 1.x-dev
projects[reference_option_limit][subdir] = contrib
projects[reference_option_limit][download][type] = git
projects[reference_option_limit][download][branch] = 7.x-1.x
projects[reference_option_limit][download][revision] = 0ea5303
; Provides Organic Groups integration.
projects[reference_option_limit][patch][1986532] = http://drupal.org/files/1986532_reference_option_limit_og-5.patch
; Allow one field to affect multiple other fields.
projects[reference_option_limit][patch][1986526] = http://drupal.org/files/1986526_reference_option_limit_12.patch

projects[bean][version] = "1.7"
projects[bean][subdir] = contrib

projects[context][version] = "3.2"
projects[context][subdir] = contrib

projects[libraries][version] = "2.2"
projects[libraries][subdir] = contrib

projects[restws][version] = "2.2"
projects[restws][subdir] = contrib

projects[votingapi][version] = "2.12"
projects[votingapi][subdir] = contrib

projects[pathauto][type] = module
projects[pathauto][subdir] = contrib
projects[pathauto][download][type] = git
projects[pathauto][download][url] = http://git.drupal.org/project/pathauto.git
projects[pathauto][download][branch] = 7.x-1.x
projects[pathauto][download][revision] = 8fb39ade0a10ca473fe61b1b1197ed137d673930
projects[pathauto][patch][936222] = http://www.drupal.org/files/pathauto-persist-936222-130-pathauto-state.patch

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[redirect][subdir] = contrib
projects[redirect][version] = 1.0-rc1

projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = 1.5

projects[xmlsitemap][subdir] = contrib
projects[xmlsitemap][version] = 2.0

projects[metatag][subdir] = contrib
projects[metatag][version] = 1.1

projects[transliteration][subdir] = contrib
projects[transliteration][version] = 3.2

projects[og][subdir] = contrib
projects[og][version] = 2.7

projects[views][version] = 3.8
projects[views][subdir] = contrib
; Double translation invoked on Node Type handlers.
projects[views][patch][2133653] = http://drupal.org/files/issues/2133653-views_node_type_name-7.patch
; Stale Views integration configuration when multiple modules enabled. Facilitates install profile testing.
projects[views][patch][1979926] = http://drupal.org/files/1979926-views-reset_fetch_data-2.patch
; Support multiple of the same view/display on a page so each one can have an their own exposed form. (AJAX)
projects[views][patch][1735096] = http://drupal.org/files/1735096-views-mltiple-instance-exposed-form-8.patch

projects[views_bulk_operations][type] = module
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.2
