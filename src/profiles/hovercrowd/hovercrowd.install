<?php

/**
 * Implements hook_install().
 */
function hovercrowd_install() {

  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content'));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content'));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));

  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Set initial profile settings
  variable_set('site_default_country', 'US');
  variable_set('date_default_timezone', 'America/Chicago');

  // From standard.install
  // Add text formats.
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);

  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);

  variable_set('admin_theme', 'hc_admin_theme');
  variable_set('node_admin_theme', 1);

  // Set jQuery Update
  variable_set('jquery_update_jquery_version', '1.10');
  variable_set('jquery_update_compression_type', 'none');
  variable_set('jquery_update_jquery_admin_version', '1.7');

  // rebuild permissions
  node_access_rebuild();

  hovercrowd_create_pathauto_patterns();
  $ctypes = node_type_get_types();
  hovercrowd_install_ctype_defaults($ctypes);
}

/**
 * Enable and configure pathauto.
 */
function hovercrowd_create_pathauto_patterns() {
  // Default patterns.
  variable_set('pathauto_node_pattern', '[node:title]');
  variable_set('pathauto_taxonomy_term_pattern', '[term:vocabulary]/[term:name]');
  variable_set('pathauto_user_pattern', 'user/[user:name]');

  variable_set('pathauto_node_task_pattern', 'task/[node:title]');
  variable_set('pathauto_node_submission_pattern', 'submission/[node:title]');
  variable_set('pathauto_node_comment_pattern', 'comment/[node:title]');
  variable_set('pathauto_node_organization_pattern', 'organization/[node:title]');

  variable_set('pathauto_punctuation_hyphen', 1);
  variable_set('pathauto_case', '1');
  variable_set('pathauto_max_component_length', '100');
  variable_set('pathauto_max_length', '100');
  variable_set('pathauto_reduce_ascii', '1');
  variable_set('pathauto_separator', '-');
  variable_set('pathauto_update_action', '2');
}

/**
 * Set default values for ctypes.
 *
 * @param $ctypes
 */
function hovercrowd_install_ctype_defaults($ctypes) {
  foreach ($ctypes as $ctype) {
    $ctype = (is_object($ctype) && isset($ctype->type)) ? $ctype->type : $ctype;
    variable_set("menu_options_${ctype}", array());
    variable_set("node_options_${ctype}", array());
    variable_set("node_submitted_${ctype}", 0);
    variable_set("node_preview_${ctype}", 0);
  }
}