<?php

$databases = array();
@include dirname(__FILE__) . '/settings.db.inc';

$update_free_access = FALSE;

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

ini_set('session.gc_maxlifetime', 200000);

ini_set('session.cookie_lifetime', 2000000);

/**
 * String overrides:
 *
 * To override specific strings on your site with or without enabling the Locale
 * module, add an entry to this list. This functionality allows you to change
 * a small number of your site's default English language interface strings.
 *
$conf['locale_custom_strings_en'][''] = array(
"Can't save entity as group, because user @name can't be subscribed to group and become a manager." => "Can't save entity as group, because user @name can't be subscribed to group and become a primary contact.",
'Get group managers from group audience' => 'Get organization contacts from group audience',
'List of group managers' => 'List of organization contacts',
'Show group manager' => 'Show organization primary contact',
'Group manager: !manager' => 'Organization Primary Contact: !manager',
'Overview of the group memberships (e.g. group manager, total memebrs).' => 'Overview of the group memberships (e.g. primary contacts, total members).',
'Group manager full permissions' => 'Primary Contact full permissions',
'When enabled the group manager will have all the permissions in the group.' => 'When enabled the organization primary contact will have all the permissions in the organization.',
'Group manager default roles' => 'Organization primary contact default roles',
'Select the role(s) a group manager will be granted upon creating a new group.' => 'Select the role(s) an organization primary contact will be granted upon creating a new group.',
"You can't remove membership of the group manager" => "You can't remove membership of the organization primary contact",
'You are the group manager' => 'You are the primary contact',
'As the manager of %group, you can not leave the group.' => 'As the primary contact of %group, you can not leave the group.',
'Group' => 'Organization',
'Remove from group' => 'Remove from organization',
'Choose the state to set for the selected users in the group.' => 'Choose the state to set for the selected users in the organization.',
'Group user roles' => 'Organization user roles',
'Group permissions' => 'Organization permissions',
'Subscribe user to group' => 'Subscribe user to organization',
'Group overview' => 'Organization overview',
'Edit group' => 'Edit organization',
'Edit the group. Note: This permission controls only node entity type groups.' => 'Edit the organization. Note: This permission controls only node entity type groups.',
'Administer group' => 'Administer organization',
'Manage group members and content in the group.' => 'Manage organization members and content in the group.',
'You must select one or more groups for this content.' => 'You must select one or more organizations for this content.',
'Determine access to the group.' => 'Determine access to the organization.',
'Determine access to the group content, which may override the group settings' => 'Determine access to the organization content, which may override the organization settings',
'Add group members.' => 'Add organization members.',
'Manage the group members.' => 'Manage the organization members.',
'View the group roles.' => 'View the organization roles.',
'Edit membership in group @group' => 'Edit membership in organization @group',
'People in group @group' => 'People in organization @group',
'Roles for group @group' => 'Roles for organization @group',
'You are the group manager' => 'You are the organization manager',
'Subscribe to group' => 'Subscribe to organization',
'Unsubscribe from group' => 'Unsubscribe from organization',
'Request group membership' => 'Request organization membership',
'This is the text a user may send to the group administrators.' => 'This is the text a user may send to the organization administrators.',
'This is a closed group. Only a group administrator can add you.' => 'This is a closed organization. Only an organization administrator can add you.',
'Private group' => 'Private organization',
'Organic groups' => 'Organization',
'Manage the group roles.' => 'Manage the organization roles.',
'Manage the group permissions.' => 'Manage the organization permissions.',
'View the group permissions.' => 'View the organization permissions.',
'You cannot register to this group, as you have reached your maximum allowed subscriptions.' => 'You cannot register to this organization, as you have reached your maximum allowed subscriptions.',
'Are you sure you want to join the group %title?' => 'Are you sure you want to join the organization %title?',
'Are you sure you want to unsubscribe from the group %title?' => 'Are you sure you want to unsubscribe from the organization %title?',
);*/

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$conf['site_name'] = t('Hovercrowd');