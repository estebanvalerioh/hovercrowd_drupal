module.exports = function(grunt) {

  // Load all plugins and tasks defined by the grunt-drupal-tasks package.
  require('grunt-drupal-tasks/Gruntfile')(grunt);

  // Extend the configuration
  grunt.loadNpmTasks("grunt-extend-config");

  grunt.extendConfig({
    shell: {
      pantheon_clean: {
        command: [
          'cd <%= config.buildPaths.pantheon %>',
          'rm -Rf sites/all/',
          'rm -Rf profiles/hovercrowd'
        ].join('&&')
      },
      pantheon: {
        command: [
          'cd <%= config.buildPaths.pantheon %>',
          'git add -A *',
          'git commit -m "<%= grunt.option("message") %>"',
          'git push origin master'
        ].join('&&')
      }
    },
    copy: {
      pantheon: {
        files: [
          {
            expand: true,
            cwd: '<%= config.buildPaths.profile %>/',
            src: ['**', '!.gitkeep'],
            dest: '<%= config.buildPaths.pantheon_profiles %>'
          },
          {
            expand: true,
            cwd: '<%= config.buildPaths.themes %>/',
            src: ['**', '!.gitkeep'],
            dest: '<%= config.buildPaths.pantheon_themes %>'
          },
          {
            expand: true,
            cwd: '<%= config.buildPaths.modules %>/',
            src: ['**', '!.gitkeep'],
            dest: '<%= config.buildPaths.pantheon_modules %>'
          },
          {
            src: '<%= config.buildPaths.settings %>',
            dest: '<%= config.buildPaths.pantheon_settings %>'
          }
        ]
      }
    },
    mkdir: {
      pantheon: {
        options: {
          create: [
            '<%= config.buildPaths.pantheon_profiles %>/hovercrowd',
            '<%= config.buildPaths.pantheon_contrib %>',
            '<%= config.buildPaths.pantheon_custom %>'
          ]
        }
      }
    }
  });

  var tasksPantheon = [
    'shell:pantheon_clean',
    'mkdir:pantheon',
    'copy:pantheon',
    'shell:pantheon'
  ];

  // Register Pantheon specific tasks
  grunt.registerTask('pantheon', 'Copy and commit files to Pantheon', function(arg1){
    grunt.option('message', arg1);
    grunt.task.run(tasksPantheon);
  });
};
